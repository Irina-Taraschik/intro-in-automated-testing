

export class NumbersValidator {
  /**
   * Creates an instance of NumbersValidator.
   * @memberof NumbersValidator
   */
  constructor() {
  };
  /**
   *
   *
   * @param {Number} number number to check
   * @return {Boolean} true if element is even
   * @numberof NumbersValidator
   */
  isNumberEven(number) {
    const typeOfVariable = typeof number;
    if (typeOfVariable !== 'number') {
      // eslint-disable-next-line max-len
      throw new Error(`[${number}] is not of type "Number" it is of type "${typeOfVariable}"`);
    } else {
      return number % 2 === 0;
    }
  };

  /**
   *
   *
   * @param {Arra<Number>} arrayOfNumbers array of numbers to go through
   * @return {Arra<Number>} array of even numbers
   * @memberof NumbersValidator
   */
}
